import java.util.Scanner;

public class DoWhileExample {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String input = "";
		do {
			System.out.println("Performing the withdrawal operation");
			System.out.println("Do you want to perform another transaction");
			input = scanner.next();
		} while (input.equals("yes"));
	}
}
