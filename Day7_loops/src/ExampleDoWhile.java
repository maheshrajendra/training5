import java.util.Scanner;

public class ExampleDoWhile {

	static void add(int a, int b) {
		System.out.println(a + b);
	}

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String input = "";
		do {
			System.out.println("Enter the value of a & b");
			int a = s.nextInt();
			int b = s.nextInt();
			add(a, b);
			System.out.println("Do you want to add more numbers if yes press 'yes'");
			input = s.next();
		} while (input.equals("yes"));
		System.out.println("End of calculator programs");
	}
}
