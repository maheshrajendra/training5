package com.apple.laptopapp;

public class Laptop {
	static String brand = "Apple";
	String model; // null
	double price; // 0.0

	public static void main(String[] args) {
		System.out.println(Laptop.brand); // Apple

		Laptop laptop1 = new Laptop();
		System.out.println(laptop1.model); // null
		laptop1.model = "Macbook 2022";
		System.out.println(laptop1.model); // Macbook 2022

		laptop1.brand = "Apple INC";
		System.out.println(Laptop.brand); // Apple INC
		System.out.println(laptop1.brand); // Apple INC
	}
}
