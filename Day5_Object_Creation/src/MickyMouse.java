
public class MickyMouse {

	String color = "Red";
	char size = 'M';

	public static void main(String[] args) {
		MickyMouse m1 = new MickyMouse();
		MickyMouse m2 = new MickyMouse();

		// whenever we print object reference
		// it will print the address of the object
		System.out.println(m1);
		System.out.println(m2);

		System.out.println(m1.color); // Red
		m1.color = "Blue";
		m1.size = 'L';

		System.out.println(m1.color); // Blue
		System.out.println(m1.size); // L
	}

}
