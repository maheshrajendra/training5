
public class WaterBottle {

	String color; // null
	double price = 80;
	int size = 800;

	void displayDetails() {
		System.out.println(color + " " + size + " " + price);
	}

	public static void main(String[] args) {
		WaterBottle wb1 = new WaterBottle();
		WaterBottle wb2 = new WaterBottle();
		WaterBottle wb3 = new WaterBottle();
		WaterBottle wb4 = new WaterBottle();
		WaterBottle wb5 = new WaterBottle();
		WaterBottle wb6 = new WaterBottle();

		wb1.color = "Blue";
		wb2.color = "Red";
		wb3.color = "Green";
		wb4.color = "Orange";
		wb5.color = "Black";
		wb6.color = "Grey";

		wb1.displayDetails();
		wb2.displayDetails();
		wb3.displayDetails();
		wb4.displayDetails();
		wb5.displayDetails();
		wb6.displayDetails();

		wb2.color = "Purple";
		System.out.println("______________________");
		wb2.displayDetails();

	}

}
