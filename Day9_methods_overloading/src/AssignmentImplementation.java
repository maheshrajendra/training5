
public class AssignmentImplementation {

	static int min(int a, int b) {
		if (a < b) {
			return a;
		} else {
			return b;
		}
	}
	
	static int max(int a, int b) {
		if (a > b) {
			return a;
		} else {
			return b;
		}
	}
	
	static int cube(int n) {
		return n * n * n;
	}
	
	

	
	public static void main(String[] args) {
		int min = min(15, 76);
		System.out.println(min); // 15
		
		
		System.out.println(max(56, 78)); // 78
	}

}
