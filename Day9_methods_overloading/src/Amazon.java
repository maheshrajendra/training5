
public class Amazon {
	void pay(String upiAddress) {
		System.out.println("Accepted the payment with upiAddress " + upiAddress);
	}

	void pay(String userId, String password) {
		System.out.println("Accepted payment using netbanking " + userId);
	}

	public static void main(String[] args) {
		Amazon a = new Amazon();
		a.pay("Mahesh@icici");
	}
}
