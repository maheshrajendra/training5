
public class Phone {
	void unlock(int pin) {
		System.out.println("Unlocking the phone with pin");
	}

	void unlock(String password) {
		System.out.println("Unlocking the phone with password");
	}

	public static void main(String[] args) {
		Phone p = new Phone();
		p.unlock("Alpha@123");
	}
}
