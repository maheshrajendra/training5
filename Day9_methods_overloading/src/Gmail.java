
class Gmail {
	void login(String email, String password) {
		System.out.println("Logged in with email " + email);
	}

	void login(long phoneNumber, String password) {
		System.out.println("Logged in with phone Number " + phoneNumber);
	}

	// write one more overloaded method here
	// where you login with phone number and otp
	void login(long phoneNumber, int otp) {
		System.out.println("Logged in with phone number and otp " + phoneNumber);
	}

	public static void main(String[] args) {
		Gmail g = new Gmail();
		g.login(987654321L, "Alpha@123");
		g.login("Alpha@gmail.com", "Alpha@123");
		// g.login("Alpha@123",987654321L);
		// it will throw compile time error, as there is no login method
		// which accepts one string and one long as arguments.
	}

}
