package com.mr.interfaceexample.upiexample;

public interface UPI {
	void sendMoney();

	void receiveMoney();

	void checkBalance();
}
