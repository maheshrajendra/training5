package com.mr.abstractclassexamples.phone;

public class NewTechnologyPhone extends AbstractPhone {

	@Override
	void call() {
		System.out.println("Calling with 5g technology");
	}

	@Override
	void sms() {
		System.out.println("Sending... a digital sms");
	}

}
