package com.mr.abstractclassexamples.phone;

public class RunnerPhone {
	public static void main(String[] args) {
		AbstractPhone a = new Phone(); // upcasting[PRCO]
		a.call();
		a.sms();
		a.sos();
		
		// Runtime polymorphism: depending on the object stored in the reference,
		// the jvm will decide which class implementation to be attached to the method 
		// call at the runtime of program. 
		// [upcasting + method overriding] will help us achieve run time polymorphism
	}
}
