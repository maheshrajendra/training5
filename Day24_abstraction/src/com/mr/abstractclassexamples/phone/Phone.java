package com.mr.abstractclassexamples.phone;

public class Phone extends AbstractPhone {

	@Override
	void call() {
		System.out.println("Calling...");
	}

	@Override
	void sms() {
		System.out.println("SMS...");
	}

	
}
