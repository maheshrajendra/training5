package com.mr.abstractclassexamples.phone;

public abstract class AbstractPhone {

	abstract void call();

	abstract void sms();

	void sos() {
		System.out.println("This is a sos call");
	}

}
