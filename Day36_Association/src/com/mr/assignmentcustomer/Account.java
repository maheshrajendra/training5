package com.mr.assignmentcustomer;

public class Account {
	long accNo;
	String type;
	double balance;

	@Override
	public String toString() {
		return "Account [accNo=" + accNo + ", type=" + type + ", balance=" + balance + "]";
	}

}
