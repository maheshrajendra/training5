package com.mr.assignmentcustomer;

public class Customer {
	int id;
	String name;
	Account account;

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", account=" + account + "]";
	}

}
