package com.mr.assignmentcustomer;

public class RunnerCustomer {
	public static void main(String[] args) {
		Customer c = new Customer();
		c.id = 13;
		c.name = "Mark";
		c.account = new Account();
		c.account.accNo = 856699787797L;
		c.account.type = "Savings";
		c.account.balance = 5600;

		System.out.println(c);

		Account a2 = new Account();
		a2.accNo = 23423423423L;
		a2.type = "Current";
		a2.balance = 54000;

		Customer c2 = new Customer();
		c2.id = 12;
		c2.name = "Roger";
		c2.account = a2;
		System.out.println(c2);
	}
}
