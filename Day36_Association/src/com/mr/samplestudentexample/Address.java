package com.mr.samplestudentexample;

public class Address {
	int houseNo;
	String street;
	String area;
	int pincode;

	@Override
	public String toString() {
		return "Address [houseNo=" + houseNo + ", street=" + street + ", area=" + area + ", pincode=" + pincode + "]";
	}

}
