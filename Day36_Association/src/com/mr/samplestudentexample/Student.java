package com.mr.samplestudentexample;

public class Student {
	int rollNo;
	String name;
	long phoneNumber;
	Address address;
	
	
	@Override
	public String toString() {
		return "Student [rollNo=" + rollNo + ", name=" + name + ", phoneNumber=" + phoneNumber + ", address=" + address
				+ "]";
	}



	// when class is the datatype object of that class is the data
	public static void main(String[] args) {
		Student s = new Student();
		s.rollNo = 20;
		s.name = "Alpha";
		s.phoneNumber = 23423423423L;
		s.address = new Address();
		s.address.houseNo = 4;
		s.address.street = "1st main";
		s.address.area = "BTM";
		s.address.pincode = 560080;
		System.out.println(s);
	}
}
