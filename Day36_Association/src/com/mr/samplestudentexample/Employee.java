package com.mr.samplestudentexample;

public class Employee {
	String name;
	String empNo;
	Address address;

	@Override
	public String toString() {
		return "Employee [name=" + name + ", empNo=" + empNo + ", address=" + address + "]";
	}

	public static void main(String[] args) {
		Employee e = new Employee();
		e.name = "Charlie";
		e.empNo = "E123";
		e.address = new Address();
		e.address.houseNo = 12;
		e.address.area = "Jayanagar";
		e.address.street = "2nd main";
		e.address.pincode = 560070;
		System.out.println(e);
	}
}
