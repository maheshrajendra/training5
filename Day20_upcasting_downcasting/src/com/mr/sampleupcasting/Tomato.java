package com.mr.sampleupcasting;

public class Tomato extends Fruit {

	void prepareSauce() {
		System.out.println("Prepare Tomato Sauce");
	}
}
