package com.mr.sampleupcasting;

public class Apple extends Fruit {
	void prepareMilkShake() {
		System.out.println("Prepare apple milk shake");
	}
}
