package com.mr.sampleupcasting;

import java.util.Scanner;

public class Shop {
	Fruit sell(int option) {
		System.out.println("The option is " + option);
		if (option == 1) {
			System.out.println("Returning Apple object");
			return new Apple();
		} else {
			System.out.println("Returning Tomato object");
			return new Tomato();
		}
	}

	public static void main(String[] args) {
		System.out.println("Enter 1. Apple, any other no. for Tomato");
		Scanner s = new Scanner(System.in);
		int no = s.nextInt();
		Shop shop = new Shop();
		Fruit f = shop.sell(no); // upcasting[PRCO] happens at this line. -> Fruit f = new Apple();
		f.clean();
		// when we upcast, we cannot access the sub class specific methods, so we need
		// to downcast, at the time of downcasting we encounter ClassCastException, so
		// to avoid we make use of instanceof operator
		if (f instanceof Tomato) {
			Tomato t = (Tomato) f;
			t.prepareSauce();
		} else if (f instanceof Apple) {
			Apple a = (Apple) f;
			a.prepareMilkShake();
		}
	}
}
