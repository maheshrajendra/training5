package com.mr.sampletypecasting;

/*
 * Narrowing: converting higher primitive datatype to lower primitive datatype
 * we have to explicitly cast to the lower primitive datatype else there 
 * will be type mismatch compiler error.
 */
public class SampleNarrowing {
	public static void main(String[] args) {
		long l = 135;
		int i = (int) l;
		short s = (short) i;
		byte b = (byte) s;

		System.out.println(l + " " + i + " " + " " + s + " " + b);
	}
}
