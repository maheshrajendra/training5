package com.mr.sampletypecasting;

/*
 * Widening: converting from lower primitive datatype to higher primitive datatype
 * There is no need to explicitly cast to the higher datatype, as it is promotion
 * to the higher datatype automatically
 */
public class SampleWidening {
	public static void main(String[] args) {
		byte b = 10;
		short s = b;
		int i = s;
		long l = i;

		System.out.println(b + " " + s + " " + i + " " + l);
	}
}
