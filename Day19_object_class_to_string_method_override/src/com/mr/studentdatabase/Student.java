package com.mr.studentdatabase;

public class Student {
	int sNo;
	String name;
	long phoneNo;

	// 3 param constructor -> ALT + SHIFT + S + O
	public Student(int sNo, String name, long phoneNo) {
		this.sNo = sNo;
		this.name = name;
		this.phoneNo = phoneNo;
	}

	/*
	 * ALT + SHIFT + S + S -> override toString() 
	 * Q:       why we have to override toString()? 
	 * Ans:		Instead of address, if we have to print meaning message or content of the object we override toString()
	 */
	@Override
	public String toString() {
		return "Student [sNo=" + sNo + ", name=" + name + ", phoneNo=" + phoneNo + "]";
	}

	public static void main(String[] args) {
		Student s = new Student(1, "Alpha", 98654321L);
		// when a object reference is printed, it will indirectly invoke(call) the
		// toString() on the object
		System.out.println(s); 
	}

}
