

public class Greeting {

	static void greet(String name) {
		System.out.println("Hello " + name);
	}

	public static void main(String[] args) {
		greet(null); // Hello null
		greet("Charlie"); // Hello Charlie
		greet("Mark"); // Hello Mark
		greet("mark"); // Hello mark
	}
	// after you type your program
	// use ctrl + shift +f, then ctrl + shift + s
}
