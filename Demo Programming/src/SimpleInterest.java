
public class SimpleInterest {
	static void calculate(double p, int t, double r) {
		double interest = (p * t * r) / 100;
		System.out.println(interest);
	}

	public static void main(String[] args) {
		calculate(100000, 1, 5.5); // 5500.0
		calculate(100000, 1, 6.7); // 6700.0
	}
}
