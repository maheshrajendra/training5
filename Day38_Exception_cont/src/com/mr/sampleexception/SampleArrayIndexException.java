package com.mr.sampleexception;

public class SampleArrayIndexException {
	public static void main(String[] args) {
		int[] arr = { 1, 2, 3, 4 };
		System.out.println(arr.length);
		System.out.println(arr[-4]);
	}
}
