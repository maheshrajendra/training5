package com.mr.sampleexception;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Objects;

public class Account {
	int accId;
	String name;

	public Account(int accId, String name) {
		this.accId = accId;
		this.name = name;
	}

	@Override
	public int hashCode() {
		return Objects.hash(accId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		return accId == other.accId;
	}

	@Override
	public String toString() {
		return "Account [accId=" + accId + ", name=" + name + "]\n";
	}

	public static void main(String[] args) {
		Account a1 = new Account(123, "Alpha");
		Account a2 = new Account(546, "Beta");
		Account a3 = new Account(123, "Charlie");
		LinkedHashSet<Account> accounts = new LinkedHashSet<>(Arrays.asList(a1, a2, a3, null, null));
		System.out.println(accounts);

	}

}
