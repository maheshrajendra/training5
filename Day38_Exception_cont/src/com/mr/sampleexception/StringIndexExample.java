package com.mr.sampleexception;

import java.util.Scanner;

public class StringIndexExample {
	public static void main(String[] args) {
		try {
			System.out.println("Enter the name");
			String name = new Scanner(System.in).next();
			System.out.println(name.charAt(5));
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("End the program");
	}
}
