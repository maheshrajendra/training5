package com.mr.inheritanceexample.superchaining.bankexample;

public class AirtelPaymentsBank extends Bank {

	AirtelPaymentsBank(String branchName, String address) {
		super(branchName, address);
	}

	AirtelPaymentsBank() {
		super("Mumbai", "Thane");
	}

	public static void main(String[] args) {
		// how to create object of AirtelPaymentsBank ??????????
		AirtelPaymentsBank a = new AirtelPaymentsBank("Bangalore", "JP Nagar");
		
		AirtelPaymentsBank a2 = new AirtelPaymentsBank();
	}

}
