package com.mr.inheritanceexample.superchaining.bankexample;

public class Bank {
	String branchName;
	String address;

	// ALT + SHIFT + S + O -> to generate constructor
	Bank(String branchName, String address) {
		this.branchName = branchName;
		this.address = address;
	}

}
