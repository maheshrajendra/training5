package com.mr.inheritanceexample.superchaining;

public class Dog extends Animal {

	Dog(String type) {
		super(type);
	}

	public static void main(String[] args) {
		Dog d = new Dog("Domestic");
		System.out.println(d.type);
	}
}
