package com.rbi.upiapplication;

public class CitiBankImpl implements IUPI {

	@Override
	public void sendMoney(String upiAddress, double amount) {
		System.out.println("Sending " + amount + "rs to " + upiAddress + "using citibank");
	}

	@Override
	public double checkBalance() {
		return 600000.97;
	}

}
