package com.rbi.upiapplication;

public class RunnerUPI {
	public static void main(String[] args) {
		// upcasting[PRCO],
		// it is good practice to store the object in the interface reference
		// to achieve loose coupling
		IUPI i = new CitiBankImpl();

		// Consuming the services given by service provider.
		i.sendMoney("mahesh@xyz", 6000);
		System.out.println(i.checkBalance());
	}
}
