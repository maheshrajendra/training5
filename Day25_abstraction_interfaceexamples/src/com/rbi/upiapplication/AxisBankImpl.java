package com.rbi.upiapplication;

public class AxisBankImpl implements IUPI {

	@Override
	public void sendMoney(String upiAddress, double amount) {
		System.out.println("Sending... "+amount+" rs to "+upiAddress+" from axis bank");
	}

	@Override
	public double checkBalance() {
		return 50000.67;
	}

}
