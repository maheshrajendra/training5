package com.rbi.upiapplication;

public interface IUPI {
	void sendMoney(String upiAddress, double amount);

	double checkBalance();
}
