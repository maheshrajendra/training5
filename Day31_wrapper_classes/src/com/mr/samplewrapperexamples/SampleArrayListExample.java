package com.mr.samplewrapperexamples;

import java.util.ArrayList;

public class SampleArrayListExample {
	public static void main(String[] args) {
		ArrayList list = new ArrayList();
		list.add("Alpha");
		list.add("Beta");
		list.add("Charlie");
		list.add(10);
		list.add(20);
		list.add(10);
		list.add(20);
		list.add(false);
		list.add('C');
		list.add(4.5);

		System.out.println(list);
		System.out.println(list.size()); // 3
		System.out.println(list.isEmpty()); // false
	}
}
