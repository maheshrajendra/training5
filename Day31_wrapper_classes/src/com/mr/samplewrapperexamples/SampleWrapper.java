package com.mr.samplewrapperexamples;

public class SampleWrapper {
	static Integer x;
	static int y;

	public static void main(String[] args) {
		System.out.println(x);
		System.out.println(y);

		// unboxing
		int i = new Integer(10);

		// autoboxing
		Integer x = 10;
	}

}
