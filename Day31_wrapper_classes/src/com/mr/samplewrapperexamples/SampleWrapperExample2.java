package com.mr.samplewrapperexamples;

public class SampleWrapperExample2 {
	public static void main(String[] args) {
		int x = 10;
		System.out.println(x); // 10

		Integer i = 10;
		System.out.println(i); // 10, because toString() is already overriden
	}
}


// toString() method
// String
// StringBuffer
// StringBuilder
// Wrapper classes
