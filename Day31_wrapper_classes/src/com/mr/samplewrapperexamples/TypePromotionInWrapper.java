package com.mr.samplewrapperexamples;

public class TypePromotionInWrapper {
	static void print(byte d) {
		System.out.println(d);
	}

	static void print(short c) {
		System.out.println(c);
	}

	static void print(int c) {
		System.out.println(c);
	}

	static void print(long c) {
		System.out.println(c);
	}

	static void print(float c) {
		System.out.println(c);
	}

	static void print(double c) {
		System.out.println(c);
	}



	static void print(char c) {
		System.out.println("line 34");
		System.out.println(c);
	}

	public static void main(String[] args) {
		Character c = 'A';
		print(c);
	}
}
