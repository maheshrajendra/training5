package com.mr.sampleexceptions;

public class Dog {
	@Override
	protected void finalize() throws Throwable {
		System.out.println("Calling dog class finalize method");
	}

	public static void main(String[] args) {
		Dog d = new Dog();
		Dog d1 = new Dog();
		d = null;
		d1 = null;
		System.gc();
	}
}
