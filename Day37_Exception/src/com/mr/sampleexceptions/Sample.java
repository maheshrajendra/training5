package com.mr.sampleexceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Sample {
	public static void main(String[] args) {
		System.out.println("Enter two numbers");
		Scanner s = new Scanner(System.in);
		try {
			int x = s.nextInt(); // throw new InputMismatchException();
			int y = s.nextInt();
			System.out.println(x / y); // throw new ArithmeticException();
		} catch (ArithmeticException e) {
			System.out.println("You are dividing by zero change the denominator");
		} catch (InputMismatchException e) {
			System.out.println("Please enter a valid input and only number");
		} finally {
			s.close();
		}
		System.out.println("End of the program");
	}
}
