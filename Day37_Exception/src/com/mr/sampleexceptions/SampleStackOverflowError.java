package com.mr.sampleexceptions;

public class SampleStackOverflowError {

	// recursively calling the method will cause java.lang.StackOverflowError
	static void show() {
		System.out.println("Show method");
		display();
	}

	static void display() {
		System.out.println("Display method");
		show();
	}

	public static void main(String[] args) {
		System.out.println("main method");
		show();
	}
}
