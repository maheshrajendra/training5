package com.mr.sampleexceptions;

public class SampleCheckedException {
	static void check(String name) {
			System.out.println(name.toUpperCase());
	}

	public static void main(String[] args) {
		check(null);

			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		System.out.println("End of program");
	}
}
