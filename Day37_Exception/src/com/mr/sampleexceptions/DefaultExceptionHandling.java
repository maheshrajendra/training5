package com.mr.sampleexceptions;

public class DefaultExceptionHandling {
	static void disp() throws InterruptedException
	{
		System.out.println("Disp method started");
		throw new InterruptedException();
	}

	static void show() throws InterruptedException
	{
		System.out.println("Show method started");
		disp();
		System.out.println("Show method ended");
	}

	public static void main(String[] args)
	{
		System.out.println("main method started");
		try {
			show();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("main method ended");
	}
}
