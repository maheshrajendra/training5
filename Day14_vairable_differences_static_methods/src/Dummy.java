
public class Dummy {
	String x;
	static int y;

	void sample() {
		System.out.println("Sample method");
	}

	static void disp() {
		System.out.println("Disp method");
	}

	public static void main(String[] args) {
		Dummy.disp(); // Disp method
		Dummy d = new Dummy();
		d.sample(); // Sample method
		System.out.println(d.x); // null
	}

}
