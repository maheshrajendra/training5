
public class Simple {
	int x;
	static int y;

	void check() {
		System.out.println(x);
		System.out.println(Simple.y);
		Simple.display();
	}

	static void display() {
		Simple s = new Simple();
		s.check();
	}

	static void samp() {
		Simple.display();
		System.out.println(Simple.y);
		Simple s = new Simple();
		System.out.println(s.x);
	}
}
