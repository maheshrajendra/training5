package com.mr.samplestudentprog;

public class Car {
	String brand;

	Car(String brand) {
		this.brand = brand;
	}

	public static void main(String[] args) {
		Car c1 = new Car("BMW");
		System.out.println(c1.brand);

		Car c2 = new Car("Benz");
		System.out.println(c2.brand);

	}
}
