package com.mr.samplestudentprog;

public class Pen {
	String color;
	
	// one param custom constructor
	Pen(String color) {
		this.color = color;
	}

	public static void main(String[] args) {
		Pen p1 = new Pen("Red");

		System.out.println(p1.color); // Green

		// Pen p2 = new Pen(); -> Compiler error at this line
		// the above line will not throw a compiler error, as we have created custom constructor
		// the compiler will not give the default constructor
	}
}
