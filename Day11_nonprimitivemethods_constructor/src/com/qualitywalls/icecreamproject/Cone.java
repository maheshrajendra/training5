package com.qualitywalls.icecreamproject;

public class Cone {
	String flavour;
	char size;

	Cone(String flavour, char size) {
		this.flavour = flavour;
		this.size = size;
	}

	// a customer tells only flavour, by default we always give regular sized cone
	Cone(String flavour) {
		this(flavour,'R');
	}

	// when customer comes and asks for icecream without any inputs you must give
	// Raspberry L
	Cone() {
		this("Raspberry", 'L');
	}
	
	// ALT + SHIFT + S+ S -> to generate toString()
	@Override
	public String toString() {
		return "Cone [flavour=" + flavour + ", size=" + size + "]";
	}


	public static void main(String[] args) {
		Cone c1 = new Cone("Chocolate", 'L');
		System.out.println(c1);

		Cone c2 = new Cone("Vanilla");
		System.out.println(c2);

		Cone c3 = new Cone();
		System.out.println(c3);

		Cone c4 = new Cone("BlackCurrent");
		System.out.println(c4);
	}

}
