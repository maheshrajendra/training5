package com.apple.iphone;

public class MobilePhone {

	void unlock(int pin) {
		System.out.println("Unlocked the phone with pin");
	}

	void unlock(String password) {
		System.out.println("Unlocked the phone with password");
	}

	void unlock(FingerPrint f) {
		System.out.println("Unlocked using fingerprint");
	}

	// Pattern -

	public static void main(String[] args) {
		MobilePhone m = new MobilePhone();
		m.unlock(1234);
		m.unlock("Alpha@123");
		m.unlock(new FingerPrint());
	}
}
