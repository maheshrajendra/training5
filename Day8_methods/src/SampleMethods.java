
public class SampleMethods {

	static void concat(String s1, String s2) {
		System.out.println(s1 + " " + s2);
	}

	static double convertDollarToInr(int dollars, double rate) {
		return dollars * rate;
	}
	
	static String greet() {
		return "Hello";
	}

	public static void main(String[] args) {
		concat("Hello", "World");

		System.out.println(convertDollarToInr(10, 75.6)); // 756.0

		System.out.println(convertDollarToInr(5436, 75.6)); // 410961.6

		concat("Mike", "Charlie"); // Mike Charlie

		System.out.println(greet()); // Hello
		
	}

}
