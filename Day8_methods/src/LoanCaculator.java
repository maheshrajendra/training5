
public class LoanCaculator {

	static double findSimpleInterest(double p, int t, double r) {
		return (p * t * r) / 100;
	}

	// sysout -> ctrl+space will give the print statement
	// main -> ctrl+space will give the main method
	public static void main(String[] args) {
		double res = findSimpleInterest(100000, 1, 7.3);
		System.out.println(res);
	}

}
