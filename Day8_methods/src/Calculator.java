
public class Calculator {

	// add two integers and return the sum of two integers
	int add(int num1, int num2) {
		int sum = num1 + num2;
		return sum;
	}
	
	// find the square of a given number
	int square(int num) {
		int value = num * num;
		return value;
	}

	public static void main(String[] args) {
		Calculator c = new Calculator();
		
		int result = c.add(15, 30);
		System.out.println(result); // 45

		result = c.add(100, 200);
		System.out.println(result); // 300

		System.out.println(c.add(25, 35)); // 60
		
		
		System.out.println(c.square(4)); // 16
	}

}
