package com.mr.encapsulationexamples.bankexample;

public class RunnerBank {
	public static void main(String[] args) {
		BankAccount bankAccount = new BankAccount();
		// unable to set account number because it is write protected
		//bankAccount.setAccountNumber(234567890L);
		bankAccount.setName("Alpha");
		bankAccount.setBalance(50000.0);

		System.out.println(bankAccount.getName());
		System.out.println(bankAccount.getAccountNumber());
		System.out.println(bankAccount.getBalance());
	}
}
