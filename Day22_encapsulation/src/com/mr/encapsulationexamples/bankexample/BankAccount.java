package com.mr.encapsulationexamples.bankexample;

import java.util.Random;

public class BankAccount {
	private String name;
	private double balance;
	private long accountNumber;
	private int age;
	private long customerId;

	public BankAccount() {
		// we are making use of predefined java.util.Random class, to generate random long value
		Random r = new Random();
		long temp = r.nextLong();
		if (temp < 0) {
			// if the number is negative, we are converting to positive
			accountNumber = temp * -1;
		} else {
			accountNumber = temp;
		}
		// alternative to the above lines
		// accountNumber = Math.abs(temp);
	}

	public int getAge() {
		return age;
	}

	// if age is >= 16 then set the age else set 0
	public void setAge(int age) {
		if (age >= 16) {
			this.age = age;
		}
	}

	// ALT +SHIFT + S + R -> to generate getters/setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

}
