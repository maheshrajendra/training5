package com.mr.encapsulationexamples.bankexample;

import java.util.Random;

public class Dice {
	public static void main(String[] args) {
		System.out.println("Spinning the dice...");
		Random r = new Random();
		System.out.println(r.nextInt(6) + 1);
	}
}
