package com.mr.encapusulationexample.employee;

public class RunnerEmployee {
	public static void main(String[] args) {
		Employee e = new Employee();
		e.setId(200);
		e.setName("Alpha");
		e.setSalary(50000);
		System.out.println(e);

		Employee e1 = new Employee("Charlie", 300, 60000);
		System.out.println(e1);
	}
}
