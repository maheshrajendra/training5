package com.mr.finalexample;

public class Sample {
	// once the value is initialized for final variable, you cannot change it.
	final int X = 10;
	final double MAX_VALUE = 10.7;

	public static void main(String[] args) {
		final String y;
		y = "Hello";
		// y = "World";
	}
}
