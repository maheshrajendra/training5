package com.mr.abstraction;

public abstract class Dummy {

	// abstract method: a method which has only declaration but no implementation,
	// preceded with keyword abstract
	abstract void show();

	public static void main(String[] args) {
		// we cannot create object of abstract class
		// Dummy d = new Dummy();
	}
}
