package com.mr.samplearraylist;

import java.util.ArrayList;

public class Sample {
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(null);
		list.add(null);
		list.add(40);
		System.out.println(list);

		System.out.println(list.get(4)); // null

		for (Integer i : list) {
			System.out.println(i);
		}
	}
}
