package com.mr.samplearraylist;

import java.util.ArrayList;
import java.util.Arrays;

public class Student {
	String name;
	int age;
	long phoneNumber;

	// ALT + SHIFT + S + O
	public Student(String name, int age, long phoneNumber) {
		this.name = name;
		this.age = age;
		this.phoneNumber = phoneNumber;
	}

	// ALT + SHIFT + S + S
	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + ", phoneNumber=" + phoneNumber + "]\n";
	}

	public static void main(String[] args) {
		Student s1 = new Student("Alpha", 24, 98765432367L);
		Student s2 = new Student("Beta", 25, 8686868686L);
		Student s3 = new Student("Charlie", 23, 9876868769L);
		Student s4 = new Student("Delta", 22, 9536458798L);
		Student s5 = new Student("Europa", 21, 75876979768L);

		// create arraylist which holds only student type of objects
		ArrayList<Student> studentList = new ArrayList<>(Arrays.asList(s1, s2, s3, s4, s5));

		// write a foreach loop and print each student.
		for (Student s : studentList) {
			System.out.println(s);
		}

		System.out.println("______________________________________");
		System.out.println(studentList);
	}
}
