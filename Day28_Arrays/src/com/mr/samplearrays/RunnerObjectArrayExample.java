package com.mr.samplearrays;

public class RunnerObjectArrayExample {
	public static void main(String[] args) {
		// objs is an Object[] so, all the elements will be upcasted when we store in it.
		Object[] objs = new Object[2];

		Student s1 = new Student("Alpha", 24, 8765434567L);
		Employee e1 = new Employee(1, "Beta");

		objs[0] = s1;
		objs[1] = e1;

		// the toString methods are overridden, so the object will hold the new implementation given 
		// instead of object class toString() logic
		System.out.println(objs[0]); // objs[0] is holding the student object,so it will invoke student class tostring
		System.out.println(objs[1]); // objs[1] is holding the employee object,so it will invoke employee class tostring

		for (Object o : objs) {
			// the elements are accessed individually and we are downcasting to its respective type
			// to access the sub class specific members
			if (o instanceof Student) {
				Student s = (Student) o;
				System.out.println(s.age);
				System.out.println(s.name);
				System.out.println(s.phoneNo);
			} else if (o instanceof Employee) {
				Employee e = (Employee) o;
				System.out.println(e.empNo);
				System.out.println(e.name);
			}
		}
	}
}
