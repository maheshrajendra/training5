package com.mr.samplearrays;

public class Ascii {

	static void toUpperCase(char c) {
		char temp = (char) ((int) c - 32);
		System.out.println(temp);
	}

	public static void main(String[] args) {
		for (int i = 0; i <= 127; i++) {
			System.out.println(i + " " + (char) i);
		}
		
		toUpperCase('*');
		
	}
}
