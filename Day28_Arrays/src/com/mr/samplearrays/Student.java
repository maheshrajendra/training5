package com.mr.samplearrays;

import java.util.Arrays;

public class Student {
	String name;
	int age;
	long phoneNo;

	public Student(String name, int age, long phoneNo) {
		this.name = name;
		this.age = age;
		this.phoneNo = phoneNo;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + ", phoneNo=" + phoneNo + "]";
	}

	public static void main(String[] args) {
		Student s1 = new Student("Alpha", 20, 98643224234L);
		Student s2 = new Student("Beta", 25, 87654323567L);
		Student s3 = new Student("Charlie", 23, 98654323456L);

		//Student[] students = {s1, s2, s3};
		Student[] students = new Student[3];
		students[0] = s1;
		students[1] = s2;
		students[2] = s3;

		System.out.println(Arrays.toString(students));

		System.out.println("_____________________________");
		for (Student student : students) {
			System.out.println(student);
		}

	}
}
