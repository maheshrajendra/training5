package com.mr.samplearrays;

public class Employee {
	int empNo;
	String name;

	// ALt + shift + s + o
	public Employee(int empNo, String name) {
		this.empNo = empNo;
		this.name = name;
	}

	// ALt + shift + s + s
	@Override
	public String toString() {
		return "Employee [empNo=" + empNo + ", name=" + name + "]";
	}

}
