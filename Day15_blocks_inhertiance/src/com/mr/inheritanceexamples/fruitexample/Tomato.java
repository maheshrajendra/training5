package com.mr.inheritanceexamples.fruitexample;

public class Tomato extends Fruit {

	boolean isRipe;

	void prepareSauce() {
		System.out.println("Preparing Tomato sauce");
	}

	@Override
	public String toString() {
		return "Tomato [color=" + color + ", isRipe=" + isRipe + "]";
	}

}
