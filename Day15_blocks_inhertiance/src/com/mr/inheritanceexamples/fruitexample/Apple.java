package com.mr.inheritanceexamples.fruitexample;

public class Apple extends Fruit {
	boolean isImported;

	void prepareMilkShake() {
		System.out.println("Preparing apple milk shake...");
	}

	@Override
	public String toString() {
		return "Apple [color=" + color + ", isImported=" + isImported + "]";
	}

}
