package com.mr.inheritanceexamples.fruitexample;

public class RunnerFruit {
	public static void main(String[] args) {
		Apple a = new Apple();
		a.color = "Green";
		a.isImported = true;
		System.out.println(a);
		a.prepareMilkShake();
	}
}
