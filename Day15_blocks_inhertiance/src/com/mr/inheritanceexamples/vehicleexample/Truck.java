package com.mr.inheritanceexamples.vehicleexample;

public class Truck extends Vehicle {

	int loadCapacity;

	void dumpLoad() {
		System.out.println("Dumping the load...");
	}

	@Override
	public String toString() {
		return "Truck [color=" + color + ", price=" + price + ", brand=" + brand + ", loadCapacity=" + loadCapacity
				+ "]";
	}

}
