package com.mr.inheritanceexamples.vehicleexample;

public class Car extends Vehicle {
	boolean isAutomatic; // false

	void cruizeControl() {
		System.out.println("Drives on its own...");
	}

	// overriding the toString() -> ALT +SHIFT + S +S -> select
	// inherited fields and fields and choose order of your choice
	@Override
	public String toString() {
		return "Car [color=" + color + ", price=" + price + ", brand=" + brand + ", isAutomatic=" + isAutomatic + "]";
	}

}
