package com.mr.inheritanceexamples.vehicleexample;

public class RunnerVehicle {
	public static void main(String[] args) {
		Car c = new Car();
		System.out.println(c); // gives all default values as we haven't initialized
		c.color = "Blue";
		c.brand = "BMW";
		c.price = 5000000;
		c.isAutomatic = true;
		c.start(); // Vehicle starts
		c.stop(); // Vehicle stops
		System.out.println(c);
		c.cruizeControl(); // Drives on its own...

		// create one object of Bike and Truck
		Bike b = new Bike();
		System.out.println(b);
		b.color = "Black";
		b.price = 300000;
		b.brand = "KTM";
		b.isKickerAvailable = true;

		b.kickStart();
		b.start();

		System.out.println(b);
		
		Truck t = new Truck();
		t.start();
		t.dumpLoad();
	}
}
