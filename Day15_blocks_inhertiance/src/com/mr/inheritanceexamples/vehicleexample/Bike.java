package com.mr.inheritanceexamples.vehicleexample;

public class Bike extends Vehicle {
	boolean isKickerAvailable;

	void kickStart() {
		System.out.println("Kick start the bike...");
	}

	@Override
	public String toString() {
		return "Bike [color=" + color + ", price=" + price + ", brand=" + brand + ", isKickerAvailable="
				+ isKickerAvailable + "]";
	}
	
	// override toString - ALT + SHIFT + S + S
	
}
