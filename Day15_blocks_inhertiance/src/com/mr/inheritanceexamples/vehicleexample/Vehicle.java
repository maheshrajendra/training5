package com.mr.inheritanceexamples.vehicleexample;

public class Vehicle {
	String color; // false
	int price; // 0
	String brand; // false

	void start() {
		System.out.println("Vehicle starts");
	}

	void stop() {
		System.out.println("Vehicle stops");
	}
}
