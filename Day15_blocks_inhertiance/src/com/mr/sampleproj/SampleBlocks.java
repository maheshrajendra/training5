package com.mr.sampleproj;

public class SampleBlocks {

	// executes whenever we create an object,
	{
		System.out.println("Instance block 1");
	}

	{
		System.out.println("Instance block 2");
	}

	// executes at the time of class loading, it executes only once throught the
	// lifetime
	static {
		System.out.println("Static block 1");
	}

	static {
		System.out.println("Static block 2");
	}

	public static void main(String[] args) {
		System.out.println("Main method");
		SampleBlocks s1 = new SampleBlocks();
		SampleBlocks s2 = new SampleBlocks();
	}
}
