package com.mr.sampleproj;

public class Sample {
	int x;
	
	// write a loop which starts from 1 increses by 5 everytime and runs for 5000 times
	// assign the last value to x

	// instance  block
	{
		for(int i = 1; i<=5000; i+=5){
			x = i;
		}
	}
	
	// static block
	static {
		
	}
	
	public static void main(String[] args) {
		
	}
}
