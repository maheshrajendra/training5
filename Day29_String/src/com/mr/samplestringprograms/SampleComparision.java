package com.mr.samplestringprograms;

public class SampleComparision {
	public static void main(String[] args) {
		String x = "hello";
		String k = new String("hello");

		System.out.println(x == k); // false

		System.out.println(x.equals(k)); // true

		// lexicographical comparision
		System.out.println("hellomyyyy".compareTo("hellom")); // 4
		System.out.println("abcd".compareTo("dcba")); // -3
		
	}
}
