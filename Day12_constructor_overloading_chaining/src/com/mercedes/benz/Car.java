package com.mercedes.benz;

public class Car {
	String color;
	char variant;
	boolean isAutomatic;
	
	// ALT + SHIFT + S + O -> shortcut to generate constructor using fields
	public Car(String color, char variant, boolean isAutomatic) {
		this.color = color;
		this.variant = variant;
		this.isAutomatic = isAutomatic;
	}
	
	public Car(String color) {
		this(color,  'P', true);
	}
	
	// ALT + SHIFT + S + S -> to override toString()
	@Override
	public String toString() {
		return "Car [color=" + color + ", variant=" + variant + ", isAutomatic=" + isAutomatic + "]";
	}
	
	public static void main(String[] args) {
		Car c1 = new Car("Red");
		System.out.println(c1); // Car [color=Red, variant=P, isAutomatic=true]
		
		Car c2 = new Car("Blue", 'E', false);
		System.out.println(c2); // Car [color=Blue, variant=E, isAutomatic=false]
	}

	
	
	
	
	
}
