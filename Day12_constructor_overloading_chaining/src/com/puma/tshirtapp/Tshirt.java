package com.puma.tshirtapp;

public class Tshirt {
	String color;
	char size;
	double price;

	public Tshirt(String color, char size, double price) {
		this.color = color;
		this.size = size;
		this.price = price;
	}

	public Tshirt(String color) {
		this(color, '0', 0.0);
	}

	public Tshirt(char size, double price) {
		this(null, size, price);
	}

	// overriding toString()
	@Override
	public String toString() {
		return "Tshirt [color=" + color + ", size=" + size + ", price=" + price + "]";
	}

	public static void main(String[] args) {
		Tshirt t1 = new Tshirt("Red");
		System.out.println(t1);

		Tshirt t2 = new Tshirt('L', 1000);
		System.out.println(t2);
		t2.color = "Black";
		t2.price = 3000;
		System.out.println(t2);

		Tshirt t3 = new Tshirt("Blue", 'M', 500);
		System.out.println(t3);

	}

}
