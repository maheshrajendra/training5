package com.aksharainstitute.aksharaapp.dto;

public class Student {
	String name;
	long phoneNumber;
	String emailAddress;

	// ALT + SHIFT + S + S
	@Override
	public String toString() {
		return "Student [name=" + name + ", phoneNumber=" + phoneNumber + ", emailAddress=" + emailAddress + "]";
	}

	public static void main(String[] args) {
		Student s1 = new Student();
		System.out.println("Before assigning the values");
		System.out.println(s1);

		s1.name = "David";
		s1.phoneNumber = 954321456789L;

		System.out.println("After assigning the values");
		System.out.println(s1);
	}
}
