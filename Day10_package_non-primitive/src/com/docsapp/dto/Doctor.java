package com.docsapp.dto;

public class Doctor {
	String name;
	boolean isAvailable;
	long phoneNumber;
	double rating;

	public static void main(String[] args) {
		Doctor d = new Doctor();
		d.name = "Steve";
		// whenever we print the object reference
		// it print the address of the object
		System.out.println(d);
		System.out.println(d.name); // Steve
	}
}
