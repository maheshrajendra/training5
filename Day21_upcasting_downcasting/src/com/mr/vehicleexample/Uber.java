package com.mr.vehicleexample;

public class Uber {

	// whenever a method is accepting non primitive as an input, it is good practice
	// to check if it is null else there will be a NullPointerException

	// NullPointerException occurs when you perform any operation on the null value
	Vehicle rent(String name) {
		if (name != null) {
			if (name.equalsIgnoreCase("bike")) {
				return new Bike();
			} else if (name.equalsIgnoreCase("car")) {
				return new Car();
			}
		}
		return null;
	}

	public static void main(String[] args) {
		Uber uber = new Uber();
		Vehicle v = uber.rent("car"); 
		if (v != null) {     // we are avoiding a NullPointerException as v reference has chances of holding null
			v.start();
			if (v instanceof Bike) { // To avoid ClassCastException at the time of downcasting, we use instanceof operator
				Bike b = (Bike) v;
				b.kickStart();
			} else if (v instanceof Car) {
				Car c = (Car) v;
				c.cruizeControl();
			}
		} else {
			System.out.println("No such vehicle available");
		}
		System.out.println("End of program");
	}

}
