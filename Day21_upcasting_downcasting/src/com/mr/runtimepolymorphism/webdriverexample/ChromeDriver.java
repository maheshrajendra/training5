package com.mr.runtimepolymorphism.webdriverexample;

public class ChromeDriver extends WebDriver {

	@Override
	void get(String url) {
		System.out.println("Opens the given url in chrome browser");
	}

}
