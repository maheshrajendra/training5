package com.mr.runtimepolymorphism.webdriverexample;

public class FirefoxDriver extends WebDriver {

	@Override
	void get(String url) {
		System.out.println("Opens the given url in firefox browser");
	}

}
