package com.mr.methodoverridingexamples.animalexample;

public class Elephant extends Animal {

	void eat() {
		System.out.println("Eat with the help of trunk");
	}

}
