package com.mr.methodoverridingexamples.animalexample;

public class RunnerAnimal {
	public static void main(String[] args) {
		Cat c = new Cat();
		c.eat();

		Monkey m = new Monkey();
		m.eat();

		Fox f = new Fox();
		f.eat();

		Elephant e = new Elephant();
		e.eat();
	}
}
