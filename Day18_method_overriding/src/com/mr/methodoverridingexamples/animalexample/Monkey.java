package com.mr.methodoverridingexamples.animalexample;

public class Monkey extends Animal {

	void eat() {
		System.out.println("Eat with help of hands");
	}

}
