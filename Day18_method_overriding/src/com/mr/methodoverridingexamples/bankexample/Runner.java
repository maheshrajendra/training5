package com.mr.methodoverridingexamples.bankexample;

public class Runner {
	public static void main(String[] args) {
		SbiATM s = new SbiATM();
		s.withdraw();
		
		DbsATM d = new DbsATM();
		d.withdraw();
	}
}
