package com.mr.samplesetexamples;

import java.util.TreeSet;

public class SampleTreeSet {
	public static void main(String[] args) {
		TreeSet<String> t = new TreeSet<>();
		t.add("Alpha");
		t.add("Charlie");
		t.add("Beta");
		t.add("Delta");
		t.add("Mike");
		System.out.println(t);
	}
}
