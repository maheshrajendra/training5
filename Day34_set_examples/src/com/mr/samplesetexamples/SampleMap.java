package com.mr.samplesetexamples;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class SampleMap {
	public static void main(String[] args) {
		LinkedHashMap<Long, String> phoneData = new LinkedHashMap<>();
		phoneData.put(987654321L, "Airtel");
		phoneData.put(987654345L, "Jio");
		phoneData.put(987654321L, "Vi");

		System.out.println(phoneData);
		System.out.println(phoneData.size()); // 2
		System.out.println(phoneData.isEmpty()); // false
		System.out.println(phoneData.containsKey(123456L)); // false
		System.out.println(phoneData.containsValue("Reliance")); // false
		System.out.println(phoneData.get(987654321L)); // Vi

		phoneData.clear();
		
		System.out.println(phoneData.size()); // 0
		
		
	}
}
