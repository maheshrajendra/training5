package com.mr.privateaccessmodifier;

public class Sample {
	 public int x = 10;

	 public void show() {
		System.out.println("Show method");
	}

	public static void main(String[] args) {
		Sample s = new Sample();
		System.out.println(s.x); // 10
		s.show();
	}
}
