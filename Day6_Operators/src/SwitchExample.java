
public class SwitchExample {
	public static void main(String[] args) {
		int a =34;
		switch (a) {
		default:
			System.out.println("Something else");
			break;
		case 3:
			System.out.println("Three");
			break;
		case 1:
			System.out.println("one");
			break;
		case 2:
			System.out.println("two");
			break;
		case 4:
			System.out.println("Four");
		}

	}
}
