
public class SwitchWithString {
	public static void main(String[] args) {
		String val = "EVENING";
		switch (val) {
		case "MORNING":
		case "morning":
			System.out.println("Good morning");
			break;
		case "EVENING":
		case "evening":
			System.out.println("Good Evening");
		}
	}
}
