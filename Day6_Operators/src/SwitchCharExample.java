
public class SwitchCharExample {
	public static void main(String[] args) {
		char input = 'c';
		switch (input) {
		case 'Y':
		case 'y':
			System.out.println("Accept");
			break;
		case 'N':
		case 'n':
		default:
			System.out.println("Declined");
		}
	}
}
