
public class SampleOperator {
	public static void main(String[] args) {
		int a = 25;
		int b = a--; // post decrement
		System.out.println(a); // 24
		System.out.println(b); // 25
	}
}
