import java.util.HashSet;

public class SampleHashSet {
	public static void main(String[] args) {
		HashSet set = new HashSet();
		set.add(1);
		set.add(2);
		set.add("Alpha");
		set.add("Alpha");
		set.add(null);
		set.add(null);
		set.add(2);
		System.out.println(set);
	}
}
