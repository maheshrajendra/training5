package com.mr.samplehashsetexamples;

import java.util.HashSet;
import java.util.Objects;

public class Student {
	int id;
	String name;
	long phoneNumber;

	public Student(int id, String name, long phoneNumber) {
		this.id = id;
		this.name = name;
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", phoneNumber=" + phoneNumber + "]";
	}

	// alt + shift + s + h -> to override equals and hashcode
	// to avoid duplicate based on id parameter, we must override equals and
	// hashcode method
	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		return id == other.id;
	}

	public static void main(String[] args) {
		HashSet<Student> set = new HashSet<>();
		Student s1 = new Student(1, "Alpha", 97857565968L);
		Student s2 = new Student(1, "Beta", 86238462864L);
		set.add(s1);
		set.add(s2);
		System.out.println(set);
		System.out.println(set.size());
	}

}
