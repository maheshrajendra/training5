package com.mr.samplearrays;

import java.util.Arrays;

public class SampleArrayCreation {
	public static void main(String[] args) {
		int[] values = new int[4];
		values[0] = 30;
		values[1] = 60;
		values[2] = 90;
		values[3] = 120;

		System.out.println(Arrays.toString(values));

		for (int i = 0; i <= values.length - 1; i++) {
			System.out.println(values[i]);
		}

		System.out.println("____________________________________");

		for (int i = values.length - 1; i >= 0; i--) {
			System.out.println(values[i]);
		}

		System.out.println("******************************************");
		for (int value : values) {
			System.out.println(value);
		}

	}
}
