package com.mr.samplearrays;

public class SampleArrayForEach {
	public static void main(String[] args) {
		String[] names = { "Alpha", "Beta", "Charlie" };

		for (String name : names) {
			System.out.println(name);
		}

		System.out.println("**************************");

		double[] heights = { 5.6, 4, 5, 6.8, 9.4 };
		for (double h : heights) {
			System.out.println(h);
		}
	}
}
