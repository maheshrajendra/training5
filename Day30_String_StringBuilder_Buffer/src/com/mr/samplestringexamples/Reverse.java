package com.mr.samplestringexamples;

public class Reverse {
	public static void main(String[] args) {
		String s = "hello";
		StringBuilder sb = new StringBuilder(s);
		sb.reverse();
		s = sb.toString();

		System.out.println(s);
	}
}
