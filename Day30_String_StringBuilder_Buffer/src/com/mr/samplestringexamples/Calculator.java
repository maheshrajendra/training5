package com.mr.samplestringexamples;

public class Calculator {
	public static void main(String[] args) {
		System.out.println("hello");
		main(10);
		main("");
		main(3.4);
	}

	public static void main(int x) {
		System.out.println(x);
	}

	public static void main(String y) {
		System.out.println(y);
	}

	public static void main(double args) {
		System.out.println(args);
	}
}
