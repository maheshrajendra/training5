package com.mr.samplestringexamples;

public class Sample {
	public static void main(String[] args) {
		String x = "hello";
		System.out.println(x.isEmpty()); // false
		x = x.replace("ll", "*");
		System.out.println(x); // he*o
		String value = String.valueOf(4); // "4"
		System.out.println(value);

		String message = new String("hello world");
		System.out.println(message); // hello world
	}
}
