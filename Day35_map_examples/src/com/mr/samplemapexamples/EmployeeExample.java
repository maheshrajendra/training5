package com.mr.samplemapexamples;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;

public class EmployeeExample {
	public static void main(String[] args) {
		LinkedHashMap<Integer, String> empTable = new LinkedHashMap<>();

		HashMap<Integer, String> empMap = new HashMap<>(empTable);
		empTable.put(3, "Charlie");
		empTable.put(1, "Alpha");
		empTable.put(4, "Delta");
		empTable.put(2, "Beta");
		System.out.println(empTable);

		// for retrieving the keys present in the map we use keySet()
		Set<Integer> keys = empTable.keySet();
		for (Integer key : keys) {
			System.out.println(key);
		}

		// for retrieving the values present in the map we use values()
		Collection<String> values = empTable.values();
		for (String value : values) {
			System.out.println(value);
		}

		System.out.println("_______ using entrySet() _______________");
		Set<Entry<Integer, String>> entries = empTable.entrySet();
		for (Entry<Integer, String> entry : entries) {
			System.out.println(entry.getKey() + "-> " + entry.getValue());
		}

		empTable.forEach((k, v) -> {
			System.out.println(k + "-" + v);
		});

		empTable.forEach((k, v) -> System.out.println(k + "-" + v));

	}
}
