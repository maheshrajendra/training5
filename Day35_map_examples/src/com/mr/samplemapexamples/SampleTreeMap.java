package com.mr.samplemapexamples;

import java.util.Collections;
import java.util.TreeMap;

public class SampleTreeMap {
	public static void main(String[] args) {
		TreeMap<Integer, String> t = new TreeMap<>(Collections.reverseOrder());
		t.put(10, "Charlie");
		t.put(4, "Beta");
		t.put(6, "Alpha");
		System.out.println(t);
	}
}

// shortcuts
// import -> ctrl+shift+O
// left hand assignment -> ctrl+1 enter

