package com.mr.samplemapexamples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class SampleArrayList {
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>(Arrays.asList(3, 2, 1, 4, 5, 6, 8, 7));

		Collections.sort(list, Collections.reverseOrder());

		System.out.println(list);
	}
}
